package se.theroxburyguys.game.example;

import java.io.IOException;
import java.util.List;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.Key;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonObjectParser;

public class GoogleHttpExample {

	static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
	static final JsonFactory JSON_FACTORY = new JacksonFactory();

	public static void main(String[] args) {
		try {
			run();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/** URL for API. */
	public static class MyUrl extends GenericUrl {

		public MyUrl(String encodedUrl) {
			super(encodedUrl);
		}

		@Key
		public String fields;
	}

	private static void run() throws Exception {
		HttpRequestFactory requestFactory = 
				HTTP_TRANSPORT.createRequestFactory(new HttpRequestInitializer() {

					public void initialize(HttpRequest request) throws IOException {
						request.setParser(new JsonObjectParser(JSON_FACTORY));

					}
				});
		//https://api.dailymotion.com/videos/?fields=id,tags,title,url
		MyUrl url = new MyUrl("https://api.dailymotion.com/videos/");
		url.fields = "id,tags,title,url";
		HttpRequest request = requestFactory.buildGetRequest(url);
		//System.out.println("" + request.execute().parseAs(VideoFeed.class));
		VideoFeed videoFeed = request.execute().parseAs(VideoFeed.class);
		if (videoFeed.list.isEmpty()) {
			System.out.println("No videos found.");
		} else {
			if (videoFeed.hasMore) {
				System.out.print("First ");
			}
			System.out.println(videoFeed.list.size() + " videos found:");
			for (Video video : videoFeed.list) {
				System.out.println();
				System.out.println("-----------------------------------------------");
				System.out.println("ID: " + video.id);
				System.out.println("Title: " + video.title);
				System.out.println("Tags: " + video.tags);
				System.out.println("URL: " + video.url);
			}
		}
	}

	/**
	 * example of json
	 * {
    "page": 1,
    "limit": 10,
    "explicit": false,
    "total": 150993583,
    "has_more": true,
    "list": [
        {
            "id": "x67skgm",
            "tags": [
                "Cracking",
                "the",
                "Calculus"
            ],
            "title": "FREE DOWNLOAD Cracking the AP Calculus AB and BC Exams, 2006-2007 Edition (College Test",
            "url": "http://www.dailymotion.com/video/x67skgm"
        }
        ]
      }
        
	 */

	/** Represents a video feed. */
	public static class VideoFeed {
		@Key
		public List<Video> list;

		@Key("has_more")
		public boolean hasMore;
	}

	/** Represents a video. */
	public static class Video {
		@Key
		public String id;

		@Key
		public List<String> tags;

		@Key
		public String title;

		@Key
		public String url;
	}

}
