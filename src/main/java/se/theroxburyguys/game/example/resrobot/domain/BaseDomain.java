package se.theroxburyguys.game.example.resrobot.domain;

import com.google.api.client.json.GenericJson;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;

public abstract class BaseDomain extends GenericJson{
	public static final JsonFactory JSON_FACTORY = new JacksonFactory();
	public BaseDomain(){
		this.setFactory(JSON_FACTORY);
	}

}
