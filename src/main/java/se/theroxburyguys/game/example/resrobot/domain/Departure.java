package se.theroxburyguys.game.example.resrobot.domain;

import com.google.api.client.json.GenericJson;
import com.google.api.client.util.Key;

public class Departure extends GenericJson {
	@Key
	public String stop;
	@Key
	public String direction;
	
	@Key(value="Product")
	public Product product;
	
	@Key(value="Stops")
	public Stops stops;
	
}
