package se.theroxburyguys.game.example.resrobot.domain;

import com.google.api.client.json.GenericJson;
import com.google.api.client.util.Key;

public class Stop extends GenericJson {
	
	@Key
	public String id;
	
	@Key
	public String name;
}

