package se.theroxburyguys.game.example.resrobot;

import java.io.IOException;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.util.Key;

import se.theroxburyguys.game.HttpUtil;
import se.theroxburyguys.game.example.resrobot.domain.Departure;
import se.theroxburyguys.game.example.resrobot.domain.DepartureFeed;
import se.theroxburyguys.game.example.resrobot.domain.Stop;

public class ResrobotHttpClientExample {

	/** URL for API. */
	public static class MyUrl extends GenericUrl {

		public MyUrl(String encodedUrl) {
			super(encodedUrl);
		}

		@Key
		public String key;
		
		@Key
		public String id;
		
		@Key
		public String direction;
		
		@Key
		public String passlist = "0";
	}
	
	public static void run () throws IOException{
		
		HttpRequestFactory requestFactory = HttpUtil.createRequestFactory();
		MyUrl url = new MyUrl("https://api.resrobot.se/departureBoard?maxJourneys=3&format=json");
		url.key = "b79f2a71-2f91-40f2-9bec-d8123321654b";
		url.id="740018261";
		url.direction="740034047";
		
		//String respAsString = requestFactory.buildGetRequest(url).execute().parseAsString();
		//System.out.println("resp = " + respAsString);
		
		DepartureFeed feed = requestFactory.buildGetRequest(url).execute().parseAs(DepartureFeed.class);
		for(Departure dep : feed.departure) {
			//System.out.println(dep.toPrettyString());
			System.out.println(dep.product.name);
			System.out.println(dep.stop);
			System.out.println(dep.direction);
			
			if(dep.stops != null) {
				System.out.println("==================");
				for(Stop stop : dep.stops.stop) {
					System.out.println(stop.id);
					System.out.println(stop.name);
				}
			}
			
		}
		
	}
	
	public static void main(String[] args) {
		try {
			run();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
