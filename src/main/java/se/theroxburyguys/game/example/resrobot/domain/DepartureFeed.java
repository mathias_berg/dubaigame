package se.theroxburyguys.game.example.resrobot.domain;

import java.util.List;

import com.google.api.client.json.GenericJson;
import com.google.api.client.util.Key;


public class DepartureFeed extends GenericJson {

	@Key (value="Departure")
	public List<Departure> departure;
	
}
