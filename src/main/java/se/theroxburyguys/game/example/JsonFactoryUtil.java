package se.theroxburyguys.game.example;

import com.google.api.client.json.JsonFactory;

public class JsonFactoryUtil {
	//private static final JsonFactory JSON_FACTORY = new JacksonFactory();
	public static JsonFactory get() {
		return com.google.api.client.json.jackson2.JacksonFactory.getDefaultInstance();
		//return JSON_FACTORY;
	}
}
