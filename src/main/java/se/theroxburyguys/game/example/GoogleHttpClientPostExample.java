package se.theroxburyguys.game.example;

import java.util.List;

import com.google.api.client.http.ByteArrayContent;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.json.GenericJson;
import com.google.api.client.util.Key;

import se.theroxburyguys.game.HttpUtil;

public class GoogleHttpClientPostExample {

	/** URL for API. */
	public static class PostUrl extends GenericUrl {

		public PostUrl(String encodedUrl) {
			super(encodedUrl);
		}

		//@Key application/x-www-form-urlencoded
		public String key;
	}
	
	
	
	public static void run () throws Exception{
		Video video = new Video();
		video.id = "dd";
		video.title = "jihoo";
		//StringWriter pw = new StringWriter();
		//System.out.println(video.getFactory().createJsonGenerator(pw));
		String json = video.toPrettyString();
		
		HttpRequestFactory requestFactory = HttpUtil.createRequestFactory();
		PostUrl url = new PostUrl("http://jsonplaceholder.typicode.com/posts");
		url.key = "b79f2a71-2f91-40f2-9bec-d8123321654b";
		/*ByteArrayContent json = ByteArrayContent.fromString("application/json", "data: {" + 
				"    title: 'foo'," + 
				"    body: 'bar'," + 
				"    userId: 1" + 
				"  }");*/
		Video2 video2 = new Video2();
		video2.id="video2";
		video2.title="vd2";
		String json2 = JsonFactoryUtil.get().toPrettyString(video2);
		System.out.println(" ===== json2 " + json2);
		
		HttpRequest request = requestFactory.buildPostRequest(url, ByteArrayContent.fromString("application/json", json));
		request.getHeaders().setContentType("application/json");
		
		
		System.out.println(request.execute().parseAsString());
	}
	
	public static void main(String[] args) {
		try {
			run();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		
	}
	
	public static class BaseDomain extends GenericJson {
		public BaseDomain() {
			this.setFactory(JsonFactoryUtil.get());
		}
	}
	
	/** Represents a video. */
	public static class Video2 {
		
		@Key
		public String id;

		@Key
		public List<String> tags;

		@Key
		public String title;

		@Key
		public String url;
	}
	
	/** Represents a video. */
	public static class Video extends BaseDomain {
		
		@Key
		public String id;

		@Key
		public List<String> tags;

		@Key
		public String title;

		@Key
		public String url;
	}

}
