package se.theroxburyguys.game;

import org.newdawn.slick.util.pathfinding.Mover;
import org.newdawn.slick.util.pathfinding.TileBasedMap;

public class GameMap implements TileBasedMap {

	/** Indicator if a given tile has been visited during the search */
	private boolean[][] visited = null;//new boolean[WIDTH][HEIGHT];
	private int width, height = 0; 
	public GameMap(int width, int height) {
		this.width = width;
		this.height = height;
		visited = new boolean[width][height];
	}
	
	@Override
	public int getWidthInTiles() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getHeightInTiles() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void pathFinderVisited(int x, int y) {
		visited[x][y] = true;
	}

	@Override
	public boolean blocked(Mover mover, int x, int y) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public float getCost(Mover mover, int sx, int sy, int tx, int ty) {
		// TODO Auto-generated method stub
		return 0;
	}

}
