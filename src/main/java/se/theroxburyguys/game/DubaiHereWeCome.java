package se.theroxburyguys.game;

import org.newdawn.slick.util.pathfinding.AStarPathFinder;
import org.newdawn.slick.util.pathfinding.Path;
import org.newdawn.slick.util.pathfinding.PathFinder;

public class DubaiHereWeCome {

	/** The path finder we'll use to search our map */
	private PathFinder finder;
	/** The last path found for the current unit */
	private Path path;
	
	
	
	private void start() {
		finder = new AStarPathFinder(null, 500, true);
	}
	
	public static void main(String[] args) {
		DubaiHereWeCome come = new DubaiHereWeCome();
		come.start();

	}
	
}
