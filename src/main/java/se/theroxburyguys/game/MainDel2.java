package se.theroxburyguys.game;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.util.pathfinding.Path;
import org.newdawn.slick.util.pathfinding.Path.Step;

import considition.ApiUtil;
import considition.Util;
import considition.api.models.City;
import considition.api.models.GameState;
import se.theroxburyguys.game.MyPathFinding.MyPath;

public class MainDel2 {

	private List<City> closed = new ArrayList<>();
	private List<City> visit = new ArrayList<>();
	private GameState game;
	private MyPathFinding finder;
	private PathSolver pathSolver = new PathSolver();
	private String[][] matrix;
	
	private List<MyPath> myPathToDubai = new ArrayList<>();

	public MainDel2(GameState game) {
		this.game = game;
		this.finder = new MyPathFinding(game);
	}
	
	private List<String> start() {
		System.out.println("start point " + game.start.x + " " + game.start.y);
		System.out.println("end point " + game.end.x + " " + game.end.y);
		City startCity = Util.findCity(game.cities, game.start.x, game.start.y);
		//hitta 5 städer runt startcity
		visit = new ArrayList<>();
		closed.add(startCity);
		visit.add(Util.getClosestCity(game.cities, startCity, closed, visit));
		visit.add(Util.getClosestCity(game.cities, visit.get(0), closed, visit));
		visit.add(Util.getClosestCity(game.cities, visit.get(1), closed, visit));
		visit.add(Util.getClosestCity(game.cities, visit.get(2), closed, visit));
		City start = startCity;
		for(City c : visit) {
			visitCity(start, c);
			start = c;
		}
		
		City endCity = Util.findCity(game.cities, game.end.x, game.end.y);
		visitCity(start, endCity);
		
		//gå till stad med bäst poäng
		//sedan stad 4,3,2,1. 
		// kolla hur lång tid du har kvar
		// hitta 5 städer runt din position
		
		//printMatrix();
		
		List<String> solution = new ArrayList<>();
		solution = pathSolver.getResult();
		
		
		/*int x = game.start.x;
		int y = game.start.y;
		while (x < game.end.x) {
			x++;
			solution.add("TRAVEL EAST");
		}
		while (y < game.end.y) {
			y++;
			solution.add("TRAVEL SOUTH");
		}*/
		
		//printMatrix(game.map);
		
		System.out.println(solution);
		return solve();
		
	}
	
	private List<String> solve() {
		List<String> retList = new ArrayList<>();
		for(MyPath mypath : myPathToDubai) {
			retList.addAll(mypath.solution);
		}
		return retList;
	}

	private void printMatrix() {
		
		if(matrix == null) {
			buildPathMatrix();
		}
		for(int i=0; i<matrix.length;i++) {
			for(int j=0; j<matrix[i].length;j++) {
				System.out.print(" " + matrix[i][j]);
			}
			System.out.println("");
		}
		
	}
	
	
	
	
	private void visitCity(City start, City visit) {
		//Path path = finder.findPath(null, start.x, start.y, visit.x, visit.y);
		MyPath mypath = finder.myOwnFindPath(start.x, start.y, visit.x, visit.y);
		Path path = mypath.path;
		if(path == null) {
			return;
		}
		myPathToDubai.add(mypath);
		
		System.out.println("Start : " + start.name);
		System.out.println("End : " + visit.name);
		System.out.println(path);
		System.out.println(mypath.solution);
		//pathSolver.solve(game, path);
	}

	private void buildPathMatrix() {
		if(matrix == null) {
			//int startStepX = path.getStep(0).getX()+50;
			//int endStepY = path.getStep(path.getLength()-1).getY()+50;
			matrix = new String[game.map.length][game.map[0].length];
			for(int i=0; i<matrix.length;i++) {
				for(int j=0; j<matrix[i].length;j++) {
					matrix[i][j] = "0";
				}	
				
			}
		}
		
		for(MyPath mypath : myPathToDubai) {
			Path path = mypath.path;
			for(int i=0; i<path.getLength(); i++) {
				Step step = path.getStep(i);
				String mark = "X";
				if(i==0) {
					mark="S";
				} else if(i==path.getLength()-1) {
					mark="E";
				}
				matrix[step.getY()][step.getX()] = mark;
			}
		}
		
		
		//printMatrix(matrix);
	}




	public static void main(String[] args) {
		GameState game = ApiUtil.start();
		MainDel2 main = new MainDel2(game);
		
		ApiUtil.submitSolution(game, main.start());
	}
}
