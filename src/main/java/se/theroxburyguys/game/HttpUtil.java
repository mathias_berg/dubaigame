package se.theroxburyguys.game;

import java.io.IOException;

import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonObjectParser;
import com.google.api.client.json.jackson2.JacksonFactory;

public class HttpUtil {

	public static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
	public static final JsonFactory JSON_FACTORY = new JacksonFactory();
	
	public static HttpRequestFactory createRequestFactory() {
		
		HttpRequestFactory requestFactory = 
				HTTP_TRANSPORT.createRequestFactory(new HttpRequestInitializer() {

					public void initialize(HttpRequest request) throws IOException {
						request.setParser(new JsonObjectParser(JSON_FACTORY));

					}
				});
		return requestFactory;
		
	}
	
	
}
