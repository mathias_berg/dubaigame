package se.theroxburyguys.game;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.util.pathfinding.Path;
import org.newdawn.slick.util.pathfinding.Path.Step;

import considition.TravelDirection;
import considition.TravelTransport;
import considition.api.models.GameState;

public class PathSolver {

	public List<StepCommand> solution = new ArrayList<>();
	
	public class StepCommand {
		public Step from;
		public String fromTerain;
		public Step to;
		public String toTerain;
		
		private StringBuilder sb = new StringBuilder("TRAVEL");
		//private String command = "TRAVEL #DIRECTION #TRANSPORT #CITY SET_PRIMARY_TRANSPORTATION #PRIMARY";
		public void setCommandDirection(String direction) {
			sb.append(" ").append(direction);
			//command = command.replace("#DIRECTION", direction);
		}
		public void setCommandTransport(String transport) {
			sb.append(" ").append(transport);
			//command = command.replace("#TRANSPORT", transport);
		}
		public void setCommandCity(String city) {
			sb.append(" ").append(city);
			//command = command.replace("#CITY", city);
		}
		public void setCommandPrimary(String primary) {
			sb.append(" ").append(primary);
			//command = command.replace("#PRIMARY", primary);
		}
		public void setCommandTransportForTerain(char terain) {
			String transport = "";
			if(!"W".equals(String.valueOf(terain))) {
				transport = TravelTransport.CAR.name();
			}
			sb.append(" ").append(transport);
			//command = command.replace("#TRANSPORT", transport);
		}
	}
	/*
     * --- Available commands ---
     * TRAVEL [NORTH|SOUTH|WEST|EAST]
     * [BUS|TRAIN|FLIGHT] {CityName}
     * SET_PRIMARY_TRANSPORTATION [CAR|BIKE]
     */
	public void solve(GameState game, Path path) {
		
		for(int i=0; i<path.getLength(); i++) {
			Step step = path.getStep(i);
			
			
			int nextStepIndex = i + 1;
			String travelDirection = null;
			if(nextStepIndex < path.getLength()) {
				StepCommand command = new StepCommand();
				command.from = step;
				char terain = game.map[step.getX()][step.getY()];
				command.fromTerain = String.valueOf(terain);
				Step nextStep = path.getStep(nextStepIndex);
				char nextTerain = game.map[nextStep.getX()][nextStep.getY()];
				command.to = nextStep;
				command.toTerain = String.valueOf(nextTerain);
				
				if(step.getX() < nextStep.getX()) { // x1 --> x2
					travelDirection = TravelDirection.EAST.name();
				} else if(step.getX() > nextStep.getX()) { // x2 <-- x1
					travelDirection = TravelDirection.WEST.name();
				} else if(step.getY() > nextStep.getY()) { // y1 --> y2
					travelDirection = TravelDirection.NORTH.name();
				} else if(step.getY() < nextStep.getY()) { // y2 <-- y1
					travelDirection = TravelDirection.SOUTH.name();
				}
				command.setCommandDirection(travelDirection);
				//command.setCommandTransport("");
				//command.setCommandCity("");
				//command.setCommandTransportForTerain(terain);
				//command.setCommandPrimary(TravelTransport.CAR.name());
				
				System.out.println(command.sb.toString());
				solution.add(command);
			}
			
			
			
		}
		
		System.out.println("==========================");
		
	}
	
	public List<String> getResult(){
		List<String> result = new ArrayList<>();
		for(StepCommand command : this.solution) {
			result.add(command.sb.toString());
		}
		return result;
	}
	
}
