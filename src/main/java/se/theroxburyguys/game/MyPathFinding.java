package se.theroxburyguys.game;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.util.pathfinding.AStarPathFinder;
import org.newdawn.slick.util.pathfinding.Mover;
import org.newdawn.slick.util.pathfinding.Path;

import considition.api.models.GameState;

public class MyPathFinding {
	AStarPathFinder finder;
	GameMap map;
	public MyPathFinding(GameState game) {
		//this.map = new GameMap(game.map.length, game.map[0].length, game.map);
		this.finder = new AStarPathFinder(map, 500, false);
		
		
		
	}
	
	public Path findPath(Mover mover, int sx, int sy, int tx, int ty) {
		return finder.findPath(mover, sx, sy, tx, ty);
	}
	
	public MyPath myOwnFindPath(int sx, int sy, int tx, int ty) {
		int x = sx;
		int y = sy;
		Path path = new Path();
		path.appendStep(x, y);
		List<String>  solution = new ArrayList<>();
		if(x < tx) {
			while (x < tx) {
				x++;
				path.appendStep(x, y);
				solution.add("TRAVEL EAST");
			}
		} else if(x > tx) {
			while (x > tx) {
				x--;
				path.appendStep(x, y);
				solution.add("TRAVEL WEST");
			}
		}
		if(y < ty) {
			while (y < ty) {
				y++;
				path.appendStep(x, y);
				solution.add("TRAVEL SOUTH");
			}
		} else if(y > ty) {
			while (y > ty) {
				y--;
				path.appendStep(x, y);
				solution.add("TRAVEL NORTH");
			}
			
		}
		MyPath myPath = new MyPath();
		myPath.path = path;
		myPath.solution = solution;
		//System.out.println(path);
		
		
		
		/*
		int deltaX = tx - sx;
		int deltaY = ty - sy;
		if(deltaX == 0) {
			if(deltaY < 0) {
				//North
			} else {
				//South
			}
		} else {
			if(deltaX < 0 ) {
				//West
			} else {
				//East
			}
		}
		*/
		return myPath;
	}
	
	public class MyPath {
		
		public Path path;
		public List<String> solution = new ArrayList<>();
		
	}
	

}
